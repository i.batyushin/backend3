<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['field-name-1'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['field-email'])) {
  print('Заполните электронную почту.<br/>');
  $errors = TRUE;
}
if (empty($_POST['field-name-2'])) {
  print('Заполните информацию о себе через 10 лет.<br/>');
  $errors = TRUE;
}
if (empty($_POST['field-date'])) {
  print('Заполните дату рождения.<br/>');
  $errors = TRUE;
}
if (empty($_POST['check-1'])) {
  print('Поставьте ак-74.<br/>');
  $errors = TRUE;
}
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u24234';
$pass = '43523453';
$db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$Vibor = implode(',',$_POST['field-name-4']);
// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, DateOfBirth = ?, SPower = ?, After10years = ?, Pitsa = ?, OcenkaPitsi = ?, Gender = ?");
  $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['field-name-3'],$_POST['field-name-2'],$Vibor,$_POST['radio-group-2'],$_POST['radio-group-1']]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
